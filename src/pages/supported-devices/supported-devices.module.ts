import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SupportedDevicesPage } from './supported-devices';

@NgModule({
  declarations: [
    SupportedDevicesPage,
  ],
  imports: [
    IonicPageModule.forChild(SupportedDevicesPage),
  ],
})
export class SupportedDevicesPageModule {}
